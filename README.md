# LJBC-2 data types, input and output

Learn java by challenge lesson 2: learn about the data types, mathematical operations and how to use standard console input and output

## Challenge
This is the second challenge, goal, check out this project as a basis, and learn how to ask the user 2 numbers and perform different mathematical operations.

## Instructions

### Checkout this Git repository
 * clone the repository `git clone git@gitlab.com:isaac.prz/ljbc-2-java-data-types-input-output.git`
 * import it as eclipse project 


### SimpleCalculator Class
 * Execute the  SimpleCalculator as java application

```
import java.util.Scanner;

public class SimpleCalculator {
	public static void main(String[] args) {
		Scanner myConsoleScanner = new Scanner(System.in);
		int x = myConsoleScanner.nextInt();
		int y = myConsoleScanner.nextInt();
		System.out.println(x + y);
	}
}


```

 * the console will be waiting, enter one number and press enter
 * the console will be still waiting, enter a second number and press enter
 * the console will print the sum of the two numbers
 
![Run SimpleCalculator](docs/simple-calculator.png)

### Explanation
  * the `java.util.Scanner` is  simple text scanner which can parse primitive types and strings using regular expressions. We need to import it, because is not part of the standard java toolset, that is imported by default in the package `java.lang`. [More about java packages](https://docs.oracle.com/javase/tutorial/java/concepts/package.html)  
  * The scanner is initialized with as `new Scanner(System.in)` indicating that we want to scan the console input.
  * we assign the new scanner to the variable named `myConsoleScanner` so that we can use it to scan the numbers, that the user will enter in the console.
  * we enter to numbers and assign them in to variables x and y.
  * afterwards we will print in the console the sum of the numbers
  
### Java primitive types

 * check the information about [primitive types](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html) 
 * afterwards read about the [java operators](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html)
 
### Challenges

1. create a simple calculator that requires two integers from console and displays their product (multiplication)
1. create a simple calculator that requires two float numbers and displays its division
1. create a simple calculator that requires three booleans (true/false) and displays the AND,OR and Exclusive OR result
1. create a class that requires two Strings and displayes the contatenation of the two strings (one after each other) 

### upload your exercises
 * In Eclipse: select your project -> right click -> Team -> Switch to -> new Branch -> write a name for your branch and click ok
 ![new-git-branch](docs/new-git-branch.png)
 * then stage your changes on the git staging view, add a commit message and press *commit and push*. This will upload your new branch into the git repository
 ![stage-commit-push](docs/stage-commit-push.png)

## Overall Information about  _Learn java by challenge_
 This is a set of projects or challeges, ordered in order to serve as a lesson. Every lesson will progress you into your goal, learning java with focus on enterprise practises. 

## Contributing
 Feel free to contribute any commentary, constructive critique, or PR with proposed changes is wellcome

## Authors and acknowledgment
 * Isaac Perez (main author)

## License

Copyright 2021 Isaac Perez

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More information [MIT License](https://opensource.org/licenses/MIT)



